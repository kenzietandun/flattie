// import 'package:flattie/layouts/main_layout.dart';
// import 'package:flattie/layouts/owing_details_layout.dart';
// import 'package:flattie/layouts/unauthorised_layout.dart';
// import 'package:flutter/material.dart';
//
// class RouteGenerator {
//   static Route<dynamic> generateRoute(RouteSettings settings) {
//     final args = settings.arguments;
//
//     switch (settings.name) {
//       case "/login":
//         return MaterialPageRoute(builder: (_) => MainLayout());
//       default:
//         return errorRoute();
//     }
//   }
//
//   static Route<dynamic> errorRoute() {
//     return MaterialPageRoute(builder: (context) {
//       return Scaffold(
//         floatingActionButton: FloatingActionButton(
//           child: Icon(Icons.arrow_back),
//           onPressed: () {
//             Navigator.pop(context);
//           },
//         ),
//         body: Container(
//           alignment: Alignment.center,
//           child: Text(
//             "Error Page",
//             style: TextStyle(
//                 color: Colors.red, fontSize: 24, fontWeight: FontWeight.w300),
//           ),
//         ),
//       );
//     });
//   }
// }
