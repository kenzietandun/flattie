// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'id.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Id _$IdFromJson(Map<String, dynamic> json) {
  return Id(
    json['id'] as int,
  );
}

Map<String, dynamic> _$IdToJson(Id instance) => <String, dynamic>{
      'id': instance.id,
    };
