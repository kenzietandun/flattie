import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

import 'model/models.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: "http://192.168.0.239:8081/")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @POST("/api/v1/login")
  Future<LoginResponse> login(@Body() LoginRequest request);

  @POST("/api/v1/register")
  Future<Id> register(@Body() RegisterRequest request);
}
