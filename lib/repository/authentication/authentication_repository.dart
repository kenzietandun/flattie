import 'dart:async';

import 'package:flattie/api/api.dart';

enum AuthenticationStatus { unknown, authenticated, unauthenticated }

class AuthenticationRepository {
  final _controller = StreamController<AuthenticationStatus>();
  final RestClient _restClient;

  AuthenticationRepository(this._restClient);

  Stream<AuthenticationStatus> get status async* {
    await Future<void>.delayed(const Duration(seconds: 1));
    yield AuthenticationStatus.unauthenticated;
    yield* _controller.stream;
  }

  Future<void> logIn({
    required String username,
    required String password,
  }) async {
    try {
      await _restClient.login(LoginRequest(username, password));
      _controller.add(AuthenticationStatus.authenticated);
    } catch (_) {
      _controller.add(AuthenticationStatus.unauthenticated);
    }
  }

  Future<void> register(
      {required String username,
      required String password,
      required String flatCode,
      required String name}) async {
    try {
      await _restClient
          .register(RegisterRequest(username, password, name, flatCode));
      await logIn(username: username, password: password);
      _controller.add(AuthenticationStatus.authenticated);
    } catch (_) {
      _controller.add(AuthenticationStatus.unauthenticated);
    }
  }

  void logOut() {
    _controller.add(AuthenticationStatus.unauthenticated);
  }

  void dispose() {
    _controller.close();
  }
}