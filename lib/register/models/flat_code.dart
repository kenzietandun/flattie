import 'package:formz/formz.dart';

enum FlatCodeValidationError { empty }

class FlatCode extends FormzInput<String, FlatCodeValidationError> {
  const FlatCode.pure() : super.pure('');

  const FlatCode.dirty([String value = '']) : super.dirty(value);

  @override
  FlatCodeValidationError? validator(String? value) {
    return value?.isNotEmpty == true ? null : FlatCodeValidationError.empty;
  }
}
