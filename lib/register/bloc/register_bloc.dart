import 'package:flattie/repository/repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flattie/register/models/flat_code.dart';
import 'package:flattie/register/register.dart';
import 'package:formz/formz.dart';

part 'register_event.dart';

part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final AuthenticationRepository _authenticationRepository;

  RegisterBloc({required AuthenticationRepository authenticationRepository})
      : _authenticationRepository = authenticationRepository,
        super(const RegisterState());

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterUsernameChanged) {
      yield _mapRegisterUsernameChangedToState(event, state);
    } else if (event is RegisterPasswordChanged) {
      yield _mapRegisterPasswordChangedToState(event, state);
    } else if (event is RegisterNameChanged) {
      yield _mapRegisterNameChangedToState(event, state);
    } else if (event is RegisterFlatCodeChanged) {
      yield _mapRegisterFlatCodeChangedToState(event, state);
    } else if (event is RegisterFormSubmitted) {
      yield* _mapRegisterFormSubmittedToState(event, state);
    }
  }

  RegisterState _mapRegisterUsernameChangedToState(
      RegisterUsernameChanged event, RegisterState state) {
    final username = Username.dirty(event.username);
    return state.copyWith(
        username: username,
        status: Formz.validate(
            [username, state.password, state.name, state.flatCode]));
  }

  RegisterState _mapRegisterPasswordChangedToState(
      RegisterPasswordChanged event, RegisterState state) {
    final password = Password.dirty(event.password);
    return state.copyWith(
        password: password,
        status: Formz.validate(
            [state.username, password, state.name, state.flatCode]));
  }

  RegisterState _mapRegisterNameChangedToState(
      RegisterNameChanged event, RegisterState state) {
    final name = Name.dirty(event.name);
    return state.copyWith(
        name: name,
        status: Formz.validate(
            [state.username, state.password, name, state.flatCode]));
  }

  RegisterState _mapRegisterFlatCodeChangedToState(
      RegisterFlatCodeChanged event, RegisterState state) {
    final flatCode = FlatCode.dirty(event.flatCode);
    return state.copyWith(
        flatCode: flatCode,
        status: Formz.validate(
            [state.username, state.password, state.name, flatCode]));
  }

  Stream<RegisterState> _mapRegisterFormSubmittedToState(
      RegisterFormSubmitted event, RegisterState state) async* {
    if (state.status.isValidated) {
      yield state.copyWith(status: FormzStatus.submissionInProgress);
      try {
        await _authenticationRepository.register(
          username: state.username.value,
          password: state.password.value,
          flatCode: state.flatCode.value,
          name: state.name.value,
        );
        yield state.copyWith(status: FormzStatus.submissionSuccess);
      } on Exception catch (_) {
        yield state.copyWith(status: FormzStatus.submissionFailure);
      }
    }
  }
}