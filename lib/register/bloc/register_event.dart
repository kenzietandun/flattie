part of 'register_bloc.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();

  @override
  List<Object?> get props => [];
}

class RegisterUsernameChanged extends RegisterEvent {
  final String username;

  const RegisterUsernameChanged(this.username);

  @override
  List<Object?> get props => [username];
}

class RegisterPasswordChanged extends RegisterEvent {
  final String password;

  const RegisterPasswordChanged(this.password);

  @override
  List<Object?> get props => [password];
}

class RegisterNameChanged extends RegisterEvent {
  final String name;

  const RegisterNameChanged(this.name);

  @override
  List<Object?> get props => [name];
}

class RegisterFlatCodeChanged extends RegisterEvent {
  final String flatCode;

  const RegisterFlatCodeChanged(this.flatCode);

  @override
  List<Object?> get props => [flatCode];
}

class RegisterFormSubmitted extends RegisterEvent {
  const RegisterFormSubmitted();

  @override
  List<Object?> get props => [];
}
