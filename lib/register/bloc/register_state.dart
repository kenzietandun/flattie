part of 'register_bloc.dart';

class RegisterState extends Equatable {
  final FormzStatus status;
  final Name name;
  final Username username;
  final Password password;
  final FlatCode flatCode;

  const RegisterState({
    this.status = FormzStatus.pure,
    this.name = const Name.pure(),
    this.username = const Username.pure(),
    this.password = const Password.pure(),
    this.flatCode = const FlatCode.pure(),
  });

  RegisterState copyWith({
    FormzStatus? status,
    Name? name,
    Username? username,
    Password? password,
    FlatCode? flatCode,
  }) {
    return RegisterState(
      status: status ?? this.status,
      name: name ?? this.name,
      username: username ?? this.username,
      password: password ?? this.password,
      flatCode: flatCode ?? this.flatCode,
    );
  }

  @override
  List<Object?> get props => [username, password, name, flatCode];
}
